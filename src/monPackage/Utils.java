package monPackage;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Utils {
    /**
     *Cette fonction vérifie si le chemin est absolu ou non.
     * @param s
     * @return
     */
    public static boolean isAbsolu(String s) {
        Path path = Paths.get(s);
        return path.isAbsolute() ? true : false;
    }

    /**
     * Cette fonction vérifie si le chemin correspond à un répertoire ou pas.
     * @param s
     * @return
     */
    public static boolean isDirectory(String s) {
        File f;
        if (isAbsolu(s)) {
            f = new File(s);
        } else {
            f = new File(Main.currDir + "\\" + s);
        }
        return isDirectory(f);
    }

    /**
     *Cette fonction vérifie si le chemin correspond à un répertoire ou pas.
     * @param f
     * @return
     */
    public static boolean isDirectory(File f) {
        if (f.exists()) {
            if (f.isDirectory()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     *Cette fonction vérifie si le chemin correspond à un fichier ou pas.
     * @param s
     * @return
     */
    public static boolean isFile(String s) {
        File f;
        if (isAbsolu(s)) {
            f = new File(s);
        } else {
            f = new File(Main.currDir + "\\" + s);
        }
        return isFile(f);
    }

    /**
     *Cette fonction vérifie si le chemin correspond à un fichier ou pas.
     * @param f
     * @return
     */
    private static boolean isFile(File f) {
        if (f.exists()) {
            if (f.isFile()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     *Cette méthode transforme un String en File.
     * @param rep
     * @return
     */
    protected static File stringToFile(String rep) {
        File repertoire = null;
        if (isDirectory(rep) || isFile(rep)) {
            if (isAbsolu(rep)) {
                repertoire = new File(rep);
            } else {
                Path repo = Paths.get(Main.currDir.toString(), rep).normalize();
                repertoire = repo.toFile();
            }
        } else {
            System.out.println("Repertoire inexistant");
            Main.shell();
        }
        return repertoire;
    }
}