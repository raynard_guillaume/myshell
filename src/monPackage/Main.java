package monPackage;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    static Path currDir = Paths.get(System.getProperty("user.home"));
    static String os = System.getProperty("os.name");

    public static void main(String[] args) {
        shell();
    }

    /**
     * Effectue et affiche, en fonction de la saisie par l'utilisateur, la saisie correspondante.
     */
    public static void shell() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.println(currDir+ ">");
        String reponse = sc.nextLine();
        String[] decoupe = reponse.split(" ");
        int nbArgs = decoupe.length;
        try {
            switch (decoupe[0]) {
                case "mypwd":
                    MyPwd.myPwd();
                    break;
                case "myps":
                    if (os.equals("Windows 10")){
                        MyPs.myPsWindows();
                    } else {
                        MyPs.myPsLinux();
                    }
                    break;
                case "myls":
                    switch (nbArgs) {
                        case 1:
                            MyLs.exec();
                        case 2:
                            MyLs.exec(decoupe[1]);
                        case 3:
                            MyLs.exec(decoupe[1], decoupe[2]);
                        default:
                            System.out.println("Erreur !");
                            shell();
                    }
                case "mydisplay":
                    MyDisplay.mydisplay(decoupe[1]);
                    break;
                case "mycd":
                    MyCd.exec(decoupe[1]);
                    break;
                case "mymv":
                    MyMv.myMv(decoupe[1], decoupe[2]);
                    break;
                case "mycp":
                    MyCp.myCp(decoupe[1], decoupe[2]);
                    break;
                case "help":
                    Help.help();
                    break;
                case "myrm":
                    MyRemove.myRmv(decoupe[1]);
                    break;
                case "myfind":
                    switch (nbArgs){
                        case 2 :
                            MyFind.myFind(decoupe[1]);
                        case 3 :
                            MyFind.myFind(decoupe[1],decoupe[2]);
                        case 4 :
                            MyFind.myFind(decoupe[1],decoupe[2], decoupe[3]);
                        default:
                            System.out.println("Erreur !");
                            shell();
                    }
                default:
                    System.out.println("Commande inconnue");
                    shell();
            }
        } catch (ArrayIndexOutOfBoundsException | IOException i) {
            i.printStackTrace();
            System.out.println("Erreur, veuillez réessayez..");
            shell();
        }
    }
}