package monPackage;

import java.io.*;
import java.nio.file.Paths;

public class MyDisplay extends Utils {
    /**
     *Cette méthode affiche le contenu d'un fichier texte.
     * @param s
     */
    public static void mydisplay(String s) {

        File file = stringToFile(s);

        if (file.exists()) {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
                String line = reader.readLine();

                while (line != null) {
                    System.out.println(line);
                    line = reader.readLine();
                }

                reader.close();

            } catch (FileNotFoundException e) {
                System.out.println("Le fichier ne se trouve pas dans le répertoire que vous avez invoqué.");
                Main.shell();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (!file.exists()){
            System.out.println("Le fichier recherché n'existe pas.");
        }
        Main.shell();
    }
}
