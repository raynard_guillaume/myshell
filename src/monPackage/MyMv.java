package monPackage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;


import static monPackage.Utils.isAbsolu;

public class MyMv extends Utils {
    /**
     *Cette méthode déplace un fichier  d'un répertoire à un autre.
     * @param s
     * @param d
     */
    public static void myMv(String s, String d) {


        File fichierSource = stringToFile(s);
        File fichierDest1 = stringToFile(d);
        File fichierDest = new File(fichierDest1.getAbsolutePath()+ "\\" + fichierSource.getName());


        if (fichierSource.exists() & !fichierDest.exists()) {
            try {
                Files.copy(fichierSource.toPath(), fichierDest.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }

            fichierSource.delete();
        } else if (fichierDest.exists()) {
            System.out.println("Le fichier que vous voulez copier existe déjà dans le répertoire de destination.");
        } else {
            System.out.println("Le fichier recherché n'existe pas.");
        }
        Main.shell();
    }
}
