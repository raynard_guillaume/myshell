package monPackage;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.FileVisitResult.CONTINUE;

public class MyFind extends Utils {

    static public String recherche = "";
    static public int pos;

    /**
     *Cette méthode cherche un fichier à partir du répertoire courant.
     * @param start
     * @throws IOException
     */
    public static void myFind(String start) throws IOException {
        myFind(Main.currDir.toString(),start);
    }

    /**
     *Cette méthode cherche un fichier à partir du répertoire courant ou, si l'option " -R" est saisie, de manière récursive.
     * @param start
     * @param arg2
     * @throws IOException
     */
    public static void myFind(String start, String arg2) throws IOException {
        if (start.equals("-R")) {
            myFind("-R", Main.currDir.toString(), arg2);
        } else {
            myFind("-1234", start, arg2);
        }
        Main.shell();
    }

    /**
     *Cette méthode gère tous les cas possibles liés à "myfind".
     * @param rep
     * @param fic
     * @throws IOException
     */
    public static void myFind(String option, String rep, String fic)
            throws IOException {

        File repertoire = stringToFile(rep);
        if (fic.charAt(0) == '*') {
            pos = 1;
            recherche = fic.substring(1);
        } else if (fic.charAt(fic.length() - 1) == '*') {
            pos = 2;
            recherche = fic.substring(0, fic.length() - 1);
        } else {
            recherche = fic;
        }
        if (option.equals("-R")) {
            Path path;
            path = Paths.get(repertoire.getAbsolutePath());
            AffichRepert affichRepert = new AffichRepert();
            Files.walkFileTree(path, affichRepert);
            Main.shell();
        } else if (option.equals("-1234")) {

            FilenameFilter filter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    switch (pos) {
                        case 1:
                            return name.endsWith(recherche);
                        case 2:
                            return name.startsWith(recherche);
                        default:
                            return name.toString().equals(recherche);
                    }
                }
            };
            String[] fichiers = repertoire.list(filter);
            if (fichiers == null) {
                System.out
                        .println("Soit le répertoire n'existe pas, soit ça n'est pas un répertoire");
            } else {
                String filename;
                for (int i = 0; i < fichiers.length; i++) {
                    filename = fichiers[i];
                    System.out.println(filename);
                }
                if (fichiers.length == 0) {
                    System.out.println("Aucun fichier ne correspond à votre recherche.");
                }
            }

        } else {
            System.out.println("Erreur de syntaxe");
        }
        Main.shell();
    }



    public static class AffichRepert extends SimpleFileVisitor<Path> {
        public FileVisitResult visitFileFailed(Path file, IOException io) {
            return FileVisitResult.SKIP_SUBTREE;
        }

        /**
         *Parcours de dossier pour la méthode.
         * @param path
         * @param attributes
         * @return
         */
        public FileVisitResult visitFile(Path path, BasicFileAttributes attributes) {
            switch (pos) {
                case 1:
                    if (path.getFileName().toString().endsWith(recherche)) {
                        System.out.println(path.toAbsolutePath());
                        return CONTINUE;
                    }
                case 2:
                    if (path.getFileName().toString().startsWith(recherche)) {
                        System.out.println(path.toAbsolutePath());
                        return CONTINUE;
                    }
                default:
                    if (path.getFileName().toString().equals(recherche)) {
                        System.out.println(path.toAbsolutePath());
                        return CONTINUE;
                    }

            }
            return CONTINUE;
        }
    }
}