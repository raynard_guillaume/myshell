package monPackage;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyLs extends Utils {

    public static void exec() {
        exec(Main.currDir.toString());
        Main.shell();
    }

    public static void exec(String s) {
        Pattern optL = Pattern.compile("^-l$", Pattern.CASE_INSENSITIVE);
        Matcher matL = optL.matcher(s);
        if (matL.matches()) {
            exec("-l", Main.currDir.toString());
        } else {
            exec("-1234", s);
        }
        Main.shell();
    }

    public static void exec(String s, String path) {
        Pattern optL = Pattern.compile("^-l$", Pattern.CASE_INSENSITIVE);
        Matcher matL = optL.matcher(s);
        File repertoire = stringToFile(path);
        File liste[] = repertoire.listFiles();
        if (liste != null) {
            for (int i = 0; i < liste.length; i++) {
                if (s.equals("-1234")) {
                    System.out.println(liste[i].getName());
                } else if (matL.matches()) {
                    System.out.printf("%s %s %s %-2s %-10s %s %n",
                            (liste[i].canRead() ? "R" : "-"),
                            (liste[i].canWrite() ? "W" : "-"),
                            (liste[i].canExecute() ? "X" : "-"),
                            (liste[i].isDirectory() ? "D" : "-"),
                            liste[i].length(), liste[i].getName());
                }
            }
        } else {
            System.err.println("Répertoire vide");
            Main.shell();
        }
        Main.shell();
    }

}