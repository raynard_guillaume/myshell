package monPackage;

public class Help {
    public static void help(){
        System.out.println("Système d'aide de MyShell\n" +
                "Voici les commandes disponibles et leurs utilisations : \n" +
                "La commande 'mypwd' indique le répertoire courant. Elle n'a besoin d'aucun argument.\n" +
                "La commande 'myps' affiche les proccessus en cours d'utilisation. Elle n'a besoin d'aucun argument. \n" +
                "La commande 'myls' liste le repertoire courant. Elle n'a besoin d'aucun argument. \n" +
                "La commande 'myls -l' liste le répertoire avec des infos supplémentaires. \n"+
                "La commande 'mydisplay' affiche le contenu d'un fichier. Elle a besoin d'un argument correspondant au fichier qui souhaite être lu. \n" +
                "La commande 'mycd' permet de changer le répertoire courant. Elle a besoin d'un argument correspondant au répertoire vers lequel on souhaite se diriger. \n" +
                "La commande 'mymv' permet de déplacer le fichier vers un répertoire. Elle a besoin de deux arguments: le premier correspond au fichier que le souhaite déplacer et le deuxième correspond au répertoire de destination.\n" +
                "La commande 'myrm' permet de supprimer un fichier. Elle a besoin d'un argument correspondant au fichier que l'on souhaite supprimer. On peut y écrire le chemin relatif ou absolu." +
                "La commande 'mycp' permet de copier le fichier vers un répertoire. Elle a besoin de deux arguments: le premier correspond au fichier que le souhaite copier et le deuxième correspond au répertoire de destination. \n" +
                "La commande 'myfind' permet de rechercher un fichier à partir du répertoire du fichier. Elle prend deux arguments: le premier correspond au fichier recherché et le deuxième correspond au chemin absolu ou relatif correspondant au répertoire du fichier. \n" +
                "La commande 'myfind -R' permet de rechercher un fichier à partir d'un répertoire parent du répertoire où se trouve le fichier. Elle prend deux arguments: le premier correspond au fichier recherché et le deuxième au répertoire parent ou courant du fichier recherché.\n" +
                "");
        Main.shell();

    }
}