package monPackage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static monPackage.Utils.isAbsolu;

public class MyCp extends Utils{

    /**
     *Cette méthode copie un fichier d'un répertoire à un autre.
     * @param s
     * @param d
     */
    public static void myCp(String s, String d) {


        File fichierSource = stringToFile(s);
        File fichierDest1 = stringToFile(d);
        File fichierDest = new File(fichierDest1.getAbsolutePath()+ "\\" + fichierSource.getName());

        if (fichierSource.exists() & !fichierDest.exists()) {
            try {
                Files.copy(fichierSource.toPath(), fichierDest.toPath());
            } catch (
                    IOException e) {
                e.printStackTrace();
            }
        } else if (fichierDest.exists()) {
            System.out.println("Le fichier que vous voulez copier existe déjà dans le répertoire de destination.");
        } else {
            System.out.println("Le fichier recherché n'existe pas.");
        }
        Main.shell();
    }
}