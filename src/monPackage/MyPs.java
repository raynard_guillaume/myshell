package monPackage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyPs extends Utils {


	/**
	 * Affiche les processus Linux.
	 */
	public static void myPsLinux() {
		Pattern patProc = Pattern.compile("[0-9]*");
		Pattern infoProc = Pattern.compile("^((Name)|(Pid)|(VmSize)|(VmRSS))");
        Matcher matProc ;
        Matcher matInfoProc;
        File proc;
		File repertoire = new File("/proc/");
		File liste[] = repertoire.listFiles();
		for (int i = 0; i < liste.length; i++) {
			matProc = patProc.matcher(liste[i].getName());
			if (matProc.matches()) {
				proc = stringToFile("/proc/"+liste[i].getName()+"/status");
				List<String> infos = new ArrayList<String>();
				BufferedReader reader;
				try {
					reader = new BufferedReader(new InputStreamReader(new FileInputStream(proc), "UTF-8"));
					String line = reader.readLine();
					while (line != null) {
						matInfoProc = infoProc.matcher(line);
						if (matInfoProc.find()) {
							String[] decoupe = line.split("\t");
							infos.add(decoupe[1]);
						}
						line = reader.readLine();
					}
					new Processus(infos);
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.printf("%-30s%-30s%-30s%-30s \n\n","Nom", "PID", "VmRSS", "VmSize");
		for (Processus processus : Processus.listeProc) {
			for (String info : processus.getListeInfo()) {
				System.out.printf("%-30s",info);
			}
			System.out.println();
		}
		Main.shell();
	}

	/**
	 * Affiche les processus Windows.
	 */
	public static void myPsWindows() {
		try {
			String process;
			Process p = Runtime.getRuntime().exec(System.getenv("windir") + "\\system32\\" + "tasklist.exe /V");
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((process = input.readLine()) != null) {
				System.out.println(process);
			}
			input.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
		Main.shell();
	}

}
