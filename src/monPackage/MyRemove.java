package monPackage;

import java.io.File;
import java.util.Scanner;

import static monPackage.Utils.isAbsolu;

public class MyRemove extends Utils {
    /**
     *Cette méthode efface un fichier.
     * @param s
     */
    public static void myRmv(String s) {


        File file = stringToFile(s);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Etes-vous sûr de vouloir supprimer le fichier ? oui ou non.");
        String reponse = scanner.next();
        if (reponse.equals("oui")) {
            {
                file.delete();
            }
        }

        Main.shell();
    }
}