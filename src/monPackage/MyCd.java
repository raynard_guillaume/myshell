package monPackage;

import java.io.File;
import java.nio.file.Paths;

public class MyCd extends Utils {
    public static void exec(String s) {
        File repertoire = stringToFile(s);
        Main.currDir = Paths.get(repertoire.getAbsolutePath());
        Main.shell();
    }
}