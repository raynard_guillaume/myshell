package monPackage;

import java.util.ArrayList;
import java.util.List;

public class Processus {
	private List<String> listeInfo = new ArrayList<String>();
	
	public static List<Processus> listeProc = new ArrayList<Processus>();

	/**
	 *C'est le constructeur de Processus.
	 * @param infos
	 */
	public Processus(List<String> infos) {
		for (String info : infos) {
			this.listeInfo.add(info);
		}
		listeProc.add(this);
	}

	public List<String> getListeInfo() {
		return listeInfo;
	}
}
